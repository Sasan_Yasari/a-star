import sys
import pygame, pygbutton
from pygame.locals import *

#define important sizes
FPS = 30
WINDOWWIDTH = 255
WINDOWHEIGHT = 300
GRIDWIDTH=255
GRIDHEIGHT=255
CELLWIDTH=20
CELLHEIGHT=20
MARGIN=1

#define colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0) #SOURCE
RED = (255, 0, 0) #DEST
GRAY = (128,128,128) #WALL
DARKBLUE = (0,153,153) #PATH
BLUE = (204,255,255) #SEEN
PINK = (255,205,255) #PINK

# Define grid elements
SOURCE = 1
DEST = 2
WALL = 0
PATH = 3
SEEN = 4

xDst = 0
yDst = 0
xSrc = 0
ySrc = 0

grid = [ [-1]*(GRIDWIDTH/CELLWIDTH) for n in range(GRIDHEIGHT/CELLHEIGHT)] # list comprehension

global numOfClick

class Cell:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.parent = None
        self.h = 0
        self.g = 0
        self.f = 0

source = Cell(0, 0)
destination = Cell(0, 0)


class AStar(object):
    def __init__(self):
        self.opened = []
        self.closed = set()

    def next(self):
        cell = self.opened[0]
        for i in self.opened:
            if cell.f > i.f:
                cell = i
        self.opened.remove(cell)
        self.closed.add(cell)
        return cell


def clear_walls():
    for x in range(0, len(grid)):
        for y in range(0, len(grid[x])):
            if grid[x][y] == 0 or grid[x][y] == 3 or grid[x][y] == 4:
                grid[x][y] = -1


def clear_grid():
    for x in range(0, len(grid)):
        for y in range(0, len(grid[x])):
            grid[x][y] = -1


def euclidean_distance(cell):
    dx = abs(cell.x - destination.x)
    dy = abs(cell.y - destination.y)
    return pow(dx * dx + dy * dy, 0.5)


def euclidean_search():
    astar_search('eulidean')


def manhattan_distance(cell):
    dx = abs(cell.x - destination.x)
    dy = abs(cell.y - destination.y)
    return dx + dy


def manhattan_search():
    astar_search('manhattan')


def astar_search(heuristic_type):
    aStar = AStar()
    aStar.opened = get_adjacent_cells(source)
    for cell in aStar.opened:
        update_cell(source, cell, heuristic_type)
        if grid[cell.x][cell.y] != SOURCE and grid[cell.x][cell.y] != DEST:
            grid[cell.x][cell.y] = SEEN

    while aStar.opened:
        cell = aStar.next()
        if cell.x == destination.x and cell.y == destination.y:
            display_path(cell)
            break

        adjacents = get_adjacent_cells(cell)

        for adjacent in adjacents:
            if grid[adjacent.x][adjacent.y] != SOURCE and grid[adjacent.x][adjacent.y] != DEST:
                grid[adjacent.x][adjacent.y] = SEEN
            if not has(aStar.closed, adjacent):
                if has(aStar.opened, adjacent):
                    if (abs(adjacent.x - cell.x) + abs(adjacent.y - cell.y) > 1 and adjacent.g > cell.g + 1.4) or (abs(adjacent.x - cell.x) + abs(adjacent.y - cell.y) == 1 and adjacent.g > cell.g + 1):
                        update_cell(cell, adjacent, heuristic_type)
                else:
                    update_cell(cell, adjacent, heuristic_type)
                    aStar.opened.append(adjacent)


def has(arr, cell):
    for a in arr:
        if a.x == cell.x and a.y == cell.y:
            return True
    return False


def get_adjacent_cells(cell):
    cells = []
    if cell.x > 0 and grid[cell.x - 1][cell.y] != WALL:
        cells.append(Cell(cell.x - 1, cell.y))
    if cell.y > 0 and grid[cell.x][cell.y - 1] != WALL:
        cells.append(Cell(cell.x, cell.y - 1))
    if cell.x > 0 and cell.y > 0 and grid[cell.x - 1][cell.y - 1] != WALL:
        cells.append(Cell(cell.x - 1, cell.y - 1))
    if cell.x < len(grid) - 1 and grid[cell.x + 1][cell.y] != WALL:
        cells.append(Cell(cell.x + 1, cell.y))
    if cell.y < len(grid) - 1 and grid[cell.x][cell.y + 1] != WALL:
        cells.append(Cell(cell.x, cell.y + 1))
    if cell.x < len(grid) - 1 and cell.y < len(grid) - 1 and grid[cell.x + 1][cell.y + 1] != WALL:
        cells.append(Cell(cell.x + 1, cell.y + 1))
    if cell.x < len(grid) - 1 and cell.y > 0 and grid[cell.x + 1][cell.y - 1] != WALL:
        cells.append(Cell(cell.x + 1, cell.y - 1))
    if cell.x > 0 and cell.y < len(grid) - 1 and grid[cell.x - 1][cell.y + 1] != WALL:
        cells.append(Cell(cell.x - 1, cell.y + 1))
    return cells


def display_path(cell):
    while True:
        cell = cell.parent
        if cell.x == source.x and cell.y == source.y:
            break
        grid[cell.x][cell.y] = PATH


def update_cell(cell, adjacent, heuristic_type):
    if abs(cell.x - adjacent.x) + abs(cell.y - adjacent.y) > 1:
        adjacent.g = cell.g + 1.4
    else:
        adjacent.g = cell.g + 1
    if heuristic_type == 'manhattan':
        adjacent.h = manhattan_distance(adjacent)
    else:
        adjacent.h = euclidean_distance(adjacent)
    adjacent.parent = cell
    adjacent.f = adjacent.h + adjacent.g


def main():
    clear_walls()
    windowBgColor = BLACK

    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURFACE = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    pygame.display.set_caption('Search Game')

    buttonManhSearch = pygbutton.PygButton((5,260,80,30),'Manhattan')
    buttonEucliSearch = pygbutton.PygButton((87.5,260,80,30),'Euclidean')
    buttonClearGrid = pygbutton.PygButton((170,260,80,30),'Clear Grids')
    gameButtons = (buttonManhSearch, buttonEucliSearch, buttonClearGrid)

    numOfClick=0

    while True: # main game loop
        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                pygame.quit()
                sys.exit()

            if event.type == pygame.MOUSEBUTTONDOWN:
              pos = pygame.mouse.get_pos()
              xPos = pos[0]/(CELLWIDTH+MARGIN)
              yPos = pos[1]/(CELLHEIGHT+MARGIN)
              if(xPos<len(grid) and yPos<len(grid[xPos])):
                numOfClick += 1
                if(numOfClick==1):
                    grid[xPos][yPos] = 1
                    source.x = xPos
                    source.y = yPos
                elif(numOfClick==2):
                    grid[xPos][yPos] = 2
                    destination.x = xPos
                    destination.y = yPos
                elif(numOfClick>2):
                    grid[xPos][yPos] = 0



            if 'click' in buttonEucliSearch.handleEvent(event):
                if(numOfClick<2):
                    pass
                else:
                    euclidean_search()
            if 'click' in buttonManhSearch.handleEvent(event):
                if(numOfClick<2):
                    pass
                else:
                    manhattan_search()
            if 'click' in buttonClearGrid.handleEvent(event):
                clear_grid()
                numOfClick=0


        DISPLAYSURFACE.fill(windowBgColor)

        for column in range(0+MARGIN, GRIDWIDTH, CELLWIDTH+MARGIN):
          for row in range(0+MARGIN, GRIDHEIGHT, CELLHEIGHT+MARGIN):
              if(column+CELLWIDTH+MARGIN>GRIDWIDTH or row+CELLHEIGHT+MARGIN>GRIDHEIGHT):
                break
              xPos = column/(CELLWIDTH+MARGIN)
              yPos = row/(CELLHEIGHT+MARGIN)
              COLOR=WHITE
              if(grid[xPos][yPos]==WALL):
                COLOR=GRAY
              elif(grid[xPos][yPos]==SOURCE):
                COLOR=GREEN
              elif(grid[xPos][yPos]==DEST):
                COLOR=RED
              elif(grid[xPos][yPos]==PATH):
                COLOR=DARKBLUE
              elif(grid[xPos][yPos]==SEEN):
                COLOR=BLUE
              else:
                pygame.draw.rect(DISPLAYSURFACE, WHITE, [column,row,CELLWIDTH,CELLHEIGHT])
              pygame.draw.rect(DISPLAYSURFACE, COLOR, [column,row,CELLWIDTH,CELLHEIGHT])

        for b in gameButtons:
            b.draw(DISPLAYSURFACE)

        pygame.display.update()
        FPSCLOCK.tick(FPS)


if __name__ == '__main__':
    main()